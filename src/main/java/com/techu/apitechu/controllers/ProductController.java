package com.techu.apitechu.controllers;

import com.techu.apitechu.ApitechuApplication;
import com.techu.apitechu.models.ProductModel;
import org.springframework.http.converter.json.GsonBuilderUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductController {

    static final String APIBaseUrl = "/apitechu/v1";

    @GetMapping(APIBaseUrl + "/products")
     public ArrayList<ProductModel> getProducts(){
        System.out.println("GetProducts");
        return ApitechuApplication.productModels;
    }

    @GetMapping(APIBaseUrl + "/products/{id}")
    public ProductModel getProductById(@PathVariable String id){
//Aquí estamos pasando el parametro id como parámetro de la url no como query string
        System.out.println("getProductById");
        System.out.println("La ID del producto a obtener es: " + id);

        ProductModel result = new ProductModel();

        for (ProductModel product:ApitechuApplication.productModels) {
            if (product.getId().equals(id)) {
                System.out.println("Producto encontrado");
                result = product;
            }
        }

        return result;
    }

    @PostMapping(APIBaseUrl + "/products")
    public ProductModel createProduct(@RequestBody ProductModel newProduct) {
        System.out.println("createProduct");
        System.out.println("La id del producto a crear es: " + newProduct.getId());
        System.out.println("La descripción del producto a crea es: " + newProduct.getDesc());
        System.out.println("El precio del producto a crea es: " + newProduct.getPrice());

        ApitechuApplication.productModels.add(newProduct);
        return newProduct;
    }

    @PutMapping(APIBaseUrl + "/products/{id}")
    public ProductModel updateProduct(@RequestBody ProductModel product, @PathVariable String id) {
        System.out.println("updateProduct");
        System.out.println("la id del producto a actualizar en parámetro URL es " + id );
        System.out.println("la id del producto a actualizar es " + product.getId() );
        System.out.println("la descripción del producto a actualizar es " + product.getDesc() );
        System.out.println("El precio del producto a actualizar es " + product.getPrice() );

        for (ProductModel productInList: ApitechuApplication.productModels) {
            if (productInList.getId().equals(id)){
                productInList.setId(product.getId());
                productInList.setDesc(product.getDesc());
                productInList.setPrice(product.getPrice());
            }
        }
        
        return product;
    }

    @PatchMapping(APIBaseUrl + "/products/{id}")
    public ProductModel updateProduct2(@RequestBody ProductModel product, @PathVariable String id ) {
        System.out.println("updateProduct parcial");
        System.out.println("la id del producto a actualizar en parámetro URL es " + id);
        System.out.println("la id del producto a actualizar es " + product.getId());
        System.out.println("la descripción del producto a actualizar es " + product.getDesc());
        System.out.println("El precio del producto a actualizar es " + product.getPrice());

        ProductModel result = new ProductModel();

        for (ProductModel productInList : ApitechuApplication.productModels) {

            if (productInList.getId().equals(id)) {

                System.out.println("Encuentro el producto");
                result = productInList;

                if (product.getDesc() != null) {
                    System.out.println("Modifico Descripción");
                    productInList.setDesc(product.getDesc());
                }
                if (product.getPrice() > 0) {
                    System.out.println("Modifico precio");
                    productInList.setPrice(product.getPrice());
                }
            }
        }
        return result;
    }

    @DeleteMapping(APIBaseUrl + "/products/{id}")
    public ProductModel deleteProduct(@PathVariable String id) {
        System.out.println("deleteProdudt");
        System.out.println("la id del producto a borrar es: " + id);

        ProductModel result = new ProductModel();
        boolean foundCompany = false;

        for(ProductModel productInList:ApitechuApplication.productModels){
            if (productInList.getId().equals(id)){
                System.out.println("Producto encontrado");
                foundCompany = true;
                result = productInList;

            }
        }

        if (foundCompany) {
            System.out.println("Borrando producto");
            ApitechuApplication.productModels.remove(result);
        }

        return result;
    }



    //Experimento mio haciendo patch con ResquestParam
    @PatchMapping(APIBaseUrl + "/productsbis/{id}")
    public ProductModel updateProduct(@PathVariable String id, @RequestParam(value = "desc",defaultValue = "") String descripcion,
                                      @RequestParam(value = "price",defaultValue = "0") Integer precio ){
        System.out.println("updateProduct patch");
        System.out.println("Descripción: " + descripcion);
        System.out.println("Precio: " + precio);

        ProductModel result = new ProductModel();

        for (ProductModel productInList: ApitechuApplication.productModels) {
            if (productInList.getId().equals(id)){

                System.out.println("Encuentro el producto");

                result = productInList;

                if (!descripcion.equals("")) {
                    System.out.println("Modifico Descripción");
                    productInList.setDesc(descripcion);
                }
                if (precio >0 ) {
                    System.out.println("Modifico precio");
                    productInList.setPrice(precio);
                }
            }
        }

        return result;
    }


}
